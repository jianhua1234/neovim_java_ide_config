# neovim_java_ide_config

Neovim Java IDE Configuration 项目旨在为 Java 开发者提供一个高效、轻量级的集成开发环境（IDE）配置，基于强大的文本编辑器 Neovim。Neovim 以其高度可配置性和强大的插件生态系统而著称，本项目通过精心挑选和配置一系列插件，以及优化 Neovim 的设置，将其打造成为一个适合 Java 开发的强大工具。

<img src="/home/keqing/neovim_java_ide_config/cover/Screenshot_20241118_204735.png" alt="Screenshot_20241118_204735" style="zoom:75%;" />

![Screenshot_20241118_204514](/home/keqing/neovim_java_ide_config/cover/Screenshot_20241118_204514.png)

