vim.g.mapleader = " "

local keymap = vim.keymap

-- ---------- 插入模式 ---------- ---
keymap.set("i", "jk", "<ESC>")

-- ---------- 视觉模式 ---------- ---
-- 单行或多行移动
keymap.set("v", "J", ":m '>+1<CR>gv=gv")
keymap.set("v", "K", ":m '<-2<CR>gv=gv")

-- ---------- 正常模式 ---------- ---
-- 窗口
keymap.set("n", "<leader>sv", "<C-w>v") -- 水平新增窗口 
keymap.set("n", "<leader>sh", "<C-w>s") -- 垂直新增窗口

-- 取消高亮
keymap.set("n", "<leader>nh", ":nohl<CR>")

-- 切换buffer
keymap.set("n", "<F3>", ":bnext<CR>")
keymap.set("n", "<F2>", ":bprevious<CR>")

-- 调整窗口宽度
keymap.set('n', '<M-->', ':resize -1<CR>', { noremap = true, silent = true }) --垂直减小
keymap.set('n', '<M-=>', ':resize +1<CR>', { noremap = true, silent = true }) --垂直增大
keymap.set('n', '<M-.>', ':vertical resize -1<CR>', { noremap = true, silent = true }) --水平减小
keymap.set('n', '<M-,>', ':vertical resize +1<CR>', { noremap = true, silent = true }) --水平增大

-- 打开终端
keymap.set('n', '<F8>', ':terminal<CR>', {noremap = true, silent = true})

-- 退出当前会话
keymap.set('n', '<C-q>', ':q<CR>', {noremap = true, silent = true})

-- ---------- 插件 ---------- ---
-- nvim-tree
keymap.set("n" , "<leader>e", ":NvimTreeToggle<CR>")

-- dashboard
keymap.set("n", "<leader>o", ":Dashboard<CR>")

-- 格式化代码格式
keymap.set('n','<C-A-CR>',":Neoformat<CR>")
