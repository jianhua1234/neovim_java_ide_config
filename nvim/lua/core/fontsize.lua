 vim.o.guifont = "JetBrainsMono Nerd Font:h12"


-- 在调整字体大小
vim.api.nvim_set_keymap("n", "<C-=>", "<Cmd>lua vim.opt.guifont = vim.o.guifont:gsub('%d+', function(size) return tostring(tonumber(size) + 1) end)<CR>", { noremap = true })
vim.api.nvim_set_keymap("n", "<C-->", "<Cmd>lua vim.opt.guifont = vim.o.guifont:gsub('%d+', function(size) return tostring(tonumber(size) - 1) end)<CR>", { noremap = true })














